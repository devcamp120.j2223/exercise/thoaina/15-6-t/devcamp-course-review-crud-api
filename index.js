const express = require("express");
var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/CRUD_Course", function (error) {
    if (error) throw error;
    console.log('Successfully connected');
})

const reviewModel = require("./app/models/reviewModel");
const courseModel = require("./app/models/courseModel");
const courseRouter = require("./app/routers/courseRouter");
const reviewRouter = require("./app/routers/reviewRouter");

const app = new express();

app.use(express.json());

app.use(express.urlencoded({
    urlencoded: true,
}));

const port = 8000;

app.get(`/`, (req, res) => {
    let today = new Date();
    res.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()}`,
    })
})

app.use("/", courseRouter);
app.use("/", reviewRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})