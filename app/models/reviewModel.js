// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const reviewSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    starts: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
});

// export 
module.exports = mongoose.model("Review", reviewSchema);