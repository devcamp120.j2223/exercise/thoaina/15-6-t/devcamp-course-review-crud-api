const express = require("express");
const reviewMiddleware = require("../middlewares/reviewMiddleware");
const reviewRouter = express.Router();

reviewRouter.use(reviewMiddleware);

// import các hàm module controller
const { getAllReviewOfCourse, getReviewById, createReviewOfCourse, updateReviewById, deleteReviewById } = require("../controllers/reviewController");

reviewRouter.get("/reviews", getAllReviewOfCourse);

reviewRouter.get("/reviews/:reviewId", getReviewById);

reviewRouter.put("/reviews/:reviewId", updateReviewById);

reviewRouter.post("/reviews", createReviewOfCourse);

reviewRouter.delete("/reviews/:reviewId", deleteReviewById);

module.exports = reviewRouter;